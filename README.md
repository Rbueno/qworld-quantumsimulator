<font style="font-size:32px;" align="left"><b>Quantum Simulator, in Julia Programing Language</b></font><br>
<font style="font-size:20px;" align="left" ><b>Final submission,</b></font><font style="font-size:16px;" align="left" ><i>by Ricardo Bueno</i></font>
<br>


# Qworld-QuantumSimulator


As part of the project phase of Qworld 511 course, I've design a simple quantum program simulator In Julia programming language. The purpose of this project is to strengthen and review the contents of the first part of the course(Q-Bronze). 

Member: 
* Ricardo Enrique Bueno Knoop

## Repository & Presentation of the project

A powerpoint presentation with extra details can be found in the repository: [Repository & Presentation](https://gitlab.com/Rbueno/qworld-quantumsimulator)

## Outcome of the project

Outcomes of the project include:
* ***Final_Deliverable.ipynb***:  A Jupiter notebook in which can be found individual tests for methods of simulating the circuit, as well as methods form simulating Quantum Gates. Test cases can also be found in the last section of this notebook

* ***functions.jl***:  A Julia script containing all the code implemented for the quantum simulator. 

* ***Auxiliar files***:  Auxiliary files include, an image folder and the notebook presented during the MidTerm

## Discussion


### Success
The project has  successfully created a basic quantum simulator. Parallel test were run with IBM's Quantum Composer to check for results,which were successful(screenshots were attached). 

<p><u>The following functions were implemented:</u>&nbsp;</p>
<ol>
    <li>Code for Methods for quantum operators:&nbsp;<ul style="list-style-type: disc;">
            <li>Hadamard&nbsp;</li>
            <li>NOT&nbsp;</li>
            <li>Z-gate</li>
        <li>Rotations(on specified angle)</li>
            <li>CNOT(for any pair of qubits)</li>
        </ul>
    </li>
    <li>Code for Methods for Simulating the circuit:<ul style="list-style-type: disc;">
            <li>read_unitary&nbsp;</li>
            <li>read_state()</li>
            <li>observing_probabilities() </li>
            <li>execute()</li>
        </ul>
    </li>
    <li>Test cases</li>
</ol>



### Challenges

Having to implement functions of Matrix Multiplication and Tensoring(Kronecker Product) were the first challenge. This was done because the project had the restriction of not using scientific or quantum related libraries.  

Then there was the CNOT gate, for which the Matrix has to be custom generated according to which bit was the target and which the control.

Another challenge arose when having to modified string since they're immutable in Julia. For this, strings were formed as arrays of characters until no longer needed.

Finally, the final challenge worth mentioning is the randomic selection of the Quantum State's measurement. For this, I used a custom statistic library called StatsBase, since I found no way of doing this natively(without being extremely inefficient). In this library the sampling was made for which the Quantum state is treated as a weighted list. 


## Conclusion


This project has resulted in a quantum simulator program able to handle the following gates: Hadamard,NOT,Z,Rotations(on a given angle), CNOT(between to given Qbits). You can also measure the Qbits, the probabilities and execute any number given of simulations. 

All challenges were overcome, but the possibility of future and bigger work can make it necessary to re-implement some of those challenges. Future work can include adding new gates, since the methods for gates are almost generic. Like other simulators, a graphical function can also be added. 

