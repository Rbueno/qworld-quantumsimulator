#using Kronecker
#using TensorOperations
using StatsBase

const h_value = 1/sqrt(2)
const h_gate = hcat([h_value h_value ; h_value -h_value])
const x_gate = hcat([0 1; 1 0 ])
const z_gate = hcat([1 0; 0 -1 ])
const i_gate = hcat([1 0; 0 1 ])

operations=""
main_mat=[]
angles=[]

function Matrix(mat)
  y=hcat(mat)
  if !isPowerTwo(size(y,1)) || size(y,1)<2
      throw(error("\n Not a correct size. Must be 2^N "))
  end
  suma=0
  for a in y
      suma+=(a^2)
  end
  try
      if(round(suma, digits=4)<0.9999 || round(suma, digits=4)>1.001)
          throw(error("Mistake"))
      else
          return y
      end
  catch e
          println("\n The given vector is not a quatum state")
  end
end

#function AjustGateSize(gate,n)
#    retgate=gate
#    b=log2(n)
#    b=floor(b)
#    for i in 1:b-1
#        retgate=kronecker(retgate, gate)
#    end
#    return retgate
#end


function isPowerTwo(n)
    b=log2(n)
    b=floor(b)
    if( (2^b)==n)
        return true
    else
        return false
    end
end

#Please note that only MatrixMultiplication2 is being used for calculations, this one is just to debug fails in MatrixMultiplication2
function MatrixMultiplication(A,B)
    A*B
end

function Hadamard(mat,target::Vector{Int64}=[-1])
    global operations=operations * 'H'
    N=size(mat,1)
    Operator=AjustGateSize2(h_gate,N,target)
    MatrixMultiplication(Operator,mat)
end

function XGate(mat,target::Vector{Int64}=[-1])
    global operations=operations * 'X'
    N=size(mat,1)
    Operator=AjustGateSize2(x_gate,N,target)
    MatrixMultiplication(Operator,mat)
end

function ZGate(mat,target::Vector{Int64}=[-1])
    global operations=operations * 'Z'
    N=size(mat,1)
    Operator=AjustGateSize2(z_gate,N,target)
    MatrixMultiplication(Operator,mat)
end

function Rotation(mat,angle,target::Vector{Int64}=[-1])
    global operations=operations * 'R'
    global angles
    angle=angle/2
    push!(angles,angle)
    r_gate = hcat([cos(angle) -sin(angle); sin(angle) cos(angle) ])
    N=size(mat,1)
    Operator=AjustGateSize2(r_gate,N,target)
    MatrixMultiplication(Operator,mat)
end

function CNGate(mat,control,target)
    global operations=operations * 'C'
    N=size(mat,1)
    cn_gate=GenerateCNOTMatriz(N,control,target)
    Operator=cn_gate
    MatrixMultiplication(Operator,mat)
end





function ReadUnitary()
    mat=i_gate
    global operations
    print("\n Current operations chain: \t",operations)
    for op in operations
        if op =='H'
            mat=MatrixMultiplication2(h_gate,mat)
        elseif op =='X'
            mat=MatrixMultiplication2(x_gate,mat)
        elseif op =='Z'
            mat=MatrixMultiplication2(z_gate,mat)
        else
            mat=MatrixMultiplication2(i_gate,mat)
        end
    end
    return mat
end


function ReadState(mat)
    N=size(mat,1)
    b=log2(N)
    b=Int(floor(b))
    f_text="\n Current State: \t"
    for i in 0:N-1
        elem=round(mat[i+1,1],digits=3)
        if(abs(elem) >= 0.001)
            f_text=string(f_text,round(elem,digits=3),"|"*join(reverse(digits(i,base=2,pad=b)))*">"," +   ")
        end
    end
    f_text=rstrip(f_text)
    f_text=rstrip(f_text,'+')
    return f_text
end


function ObservingProbabilities(mat)
    N=size(mat,1)
    b=log2(N)
    b=Int(floor(b))
    f_text="\n Probabilities for each state: \t"
    for i in 0:N-1
        elem=round(mat[i+1,1]^2,digits=3)
        if(abs(elem) >= 0.001)
            f_text=string(f_text,round(elem,digits=3),"|"*join(reverse(digits(i,base=2,pad=b)))*">"," +   ")
        end
    end
    f_text=rstrip(f_text)
    f_text=rstrip(f_text,'+')
    return f_text
end


function GenerateCNOTMatriz(N,control,target)
    b=log2(N)
    b=Int(floor(b))
    svector = []
    svector_2 = []
    for i in 0:N-1
        push!(svector_2,reverse(digits(i,base=2,pad=b)))
        push!(svector,digits(i,base=2,pad=b))
    end
    control=control+1
    target=target+1
    for i in 1:N
        #print(svector[i][control])
        #print("\t")
        if svector[i][control]==1
            if svector[i][target]==1
                svector[i][target]=0
            elseif svector[i][target]==0
                svector[i][target]=1
            end
        end
    end
    for i in 1:N
        svector[i]=reverse(svector[i])
    end
    #print("\n")
    #print(svector_2)
    #print("\n")
    #print(svector)
    #print("\n")
    #svector_2 -> X
    #svector -> Y
    ret =zeros(Int8, N, N)
    for i in 1:N
        #print(string(parse(Int, join(svector_2[i]); base=2))*" ->"*string(parse(Int, join(svector[i]); base=2))*"\n")
        ret[parse(Int, join(svector_2[i]); base=2)+1,parse(Int, join(svector[i]); base=2)+1]=1
    end
    return ret
end




function AjustGateSize2(gate,n,targets)
    b=Int(floor(log2(n)))
    vects=[]
    for k in 1:size(targets,1)
        targets[k]=targets[k]+1
    end
    #display(targets)
    if 0 in targets
        for i in 1:b
            push!(vects,gate)
        end
    else
        for i in 1:b
            if i in targets
                push!(vects,gate)
            else
                push!(vects,i_gate)
            end
        end
    end
    vects=reverse(vects)
    #display(vects)
    retgate=vects[1]
    for i in 2:b
        retgate=KroneckerProduct(retgate,vects[i])
    end
    #display(retgate)
    return retgate
end



#function KroneckerProduct(A,B)
#    m = size(A,1)
#    n = size(B,1)
#    p = size(A,2)
#    q = size(B,2)
#    C=zeros(Float64, m*p,n*q)
#    for i in 1:m
#        for j in 1:n
#            for k in 1:p
#                for l in 1:q
#                    C[p*(i-1)+k,q*(j-1)+l] = A[i,j] * B[k,l]
#                end
#            end
#        end
#    end
#    return C
#end

#for(i=1,m, for(j=1,n, for(k=1,p, for(l=1,q,
#    r[p*(i-1)+k,q*(j-1)+l]=a[i,j]*b[k,l];

function MatrixMultiplication2(A,B)
    rowa=size(A,1)
    rowb=size(B,1)
    cola=size(A,2)
    colb=size(B,2)
    C=zeros(Float64, rowa,colb)
    for i in 1:rowa
        for j in 1:colb
            for k in 1:rowb
                C[i,j] += A[i,k] * B[k,j]
            end
        end
    end
    return C
end

function KroneckerProduct(A,B)
    rowa = size(A,1)
    rowb = size(B,1)
    cola = size(A,2)
    colb = size(B,2)
    C=zeros(Float64, rowa*rowb,cola*colb)
    for i in 1:rowa
        for j in 1:cola
            tmp=A[i,j]*B
            col_left=j-1
            col_right=cola-j
            row_down=i-1
            row_up=rowa-i
            #print("\n",i,"\t",j,"\t",col_left,"\t",col_right,"\t",row_down,"\t",row_up)
            #print("\n Tmp original",tmp)
            #print("\n")
            if col_left>0
                tmp=[zeros(Float64,rowb,col_left*colb) tmp]
                #print(tmp)
                #print("\n")
            end
            if col_right>0
                tmp=[tmp zeros(Float64,rowb,col_right*colb)]
                #print(tmp)
                #print("\n")
            end
            if row_down>0
                tmp=[zeros(Float64,rowb*row_down,size(tmp,2)); tmp]
                #print(tmp)
                #print("\n")
            end
            if row_up>0
                tmp=[tmp;zeros(Float64,rowb*row_up,size(tmp,2))]
                #print(tmp)
                #print("\n")
            end
            #print("C: ",C)
            #print("\n")
            #print("tmp final:",tmp)
            C=C+tmp
        end
    end
    return C
end


function ExecuteCircuit(mat,times)
    items, weights = GetStatesProbabilities(mat)
    #print(items,weights)
    vect=sample(items, ProbabilityWeights(weights),times,replace=true)
    sumary=StatsBase.countmap(vect)
    print("\n Resulting measuraments after:\t",times," executions\n")
    for (key, value) in sumary
        print("\t\t\t",key," => \t",value,"\n")
    end

end


function GetStatesProbabilities(mat)
    N=size(mat,1)
    b=log2(N)
    b=Int(floor(b))
    states=[]
    probs=Vector{Float64}([])
    for i in 0:N-1
        elem=round(mat[i+1,1],digits=3)
        push!(probs,round(elem^2,digits=3))
        push!(states,join(reverse(digits(i,base=2,pad=b))))
    end
    return states,probs
end

function resetCircuit()
    global operations
    global angles
    operations=""
    angles=[]
end


function QuantumProgram(b)
    global operations
    global main_mat
    global angles
    operations=""
    angles=[]
    main_mat =zeros(Float64,2^b, 1)
    main_mat[1,1]=1
    return main_mat
end
